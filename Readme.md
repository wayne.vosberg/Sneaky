# Sneaky

## What

Use [jsoup](https://jsoup.org) to parse a web site which publishes a surprise sneak preview prediction, reformat using only Gmail compatible HTML, and send it to myself via email.

## Why

Because I kept forgetting about the surprise movie and setting this tool to run as a cron job a few hours before is a good reminder. The prognosis site isn't always correct but it usually narrows down what the movie might be to 2 or 3 possibilities. And, I wanted to try [jsoup](https://jsoup.org).

## How

* Import the project into [Eclipse](http://www.eclipse.org) and export to a runnable jar.

```
$ java -jar Sneaky.jar
usage: Sneaky <url> <email>
```

I'll skip publishing the URL - it's not really relevant. The point of this project was really [jsoup](https://jsoup.org) HTML parsing.
