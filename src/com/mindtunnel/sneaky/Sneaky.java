/**
   Copyright 2018 Wayne Vosberg <wayne@mindtunnel.com>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package com.mindtunnel.sneaky;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
/**
 * @author Wayne Vosberg <wayne@mindtunnel.com>
 *
 */
public class Sneaky {
	/**
	 * @param String "key:value; key : value ;key:value;
	 * @return map of key/value pairs
	 */
	public static Map<String, String> getValues(String str) {
			String[] tokens = str.split("[;:]");
			Map<String, String> map = new HashMap<String, String>();
			for (int i=0; i<tokens.length-1; ) 
				map.put(tokens[i++].trim(), tokens[i++].trim());
			return map;
	}

	/**
	 * @param url of surprise movie prediction web site
	 */
	public static void main(String[] args) {
		
		if ( args.length != 2 ) {
			System.err.println("usage: Sneaky <url> <email>");
			System.exit(-1);
		}
		
		String url = args[0]; // the url to retrieve
		String toEmail = args[1]; // where to send the results
		StringBuilder surprise = new StringBuilder(); // the string to build the output
		
		String user = System.getProperty("user.name") + "@localhost";

		try {
			Document doc = Jsoup.connect(url).get();
			// table with predictions uses lvct2 class on each row
			Elements tr = doc.select("tr.lvct2"); 

			// start the output
			surprise.append(
				"<head>\n"
				+ "<style>\n"
				+ "	table, th, td {\n"
				+ "		border-collapse: collapse;\n"
				+ "		border-style: ridge;\n"
				+ "		text-align: left;\n"	
				+ "		background-color: ghostwhite;\n"	
				+ "	}\n"
				+ "</style>\n"
				+ "</head>\n<body>\n");

			surprise.append( "<p>From <a href=" + url + " target=new>Score 11</a></p>\n"
							+ "<table>\n"
							+ "\t<tr><th>Prognosis</th><th>Movie</th><th>Opening</th></tr>\n"
							);

			// for each row, create a gmail compatible table row
			for (Element r : tr) {
				Elements td = r.select("td"); // the cell contents in this row

				// the first column contains a bar of width 1::100 which is the
				// odds of this being the surprise movie. The width is set by
				// the style applied to the div holding the cell contents.
				Element meter = td.get(1).select("div").first(); 
				String odds = meter.attr("style");
				Map<String, String> myKeys = getValues(odds);
				String width = myKeys.get("width").toString();

				// replace the style of the first column with one that is gmail 
				// compatible.
				meter.attr("style","background:cyan;height:13px;width:" + width + "px;");
				
				// the 3rd column contains a link back to the movie description.
				// we need to replace the href with an absolute URL.
				// also, remove the class on this cell (not needed).
				String href = td.get(3).select("a").first().attr("abs:href");
				Element movie = td.get(3).select("a").first().attr("href", href)
						.removeClass("lvlmt");

				// retrieve just the text of the movie description and the days till
				// opening
				String description = td.get(3).textNodes().get(0).text().trim();
				String opening = td.get(9).textNodes().get(0).text().trim();

				// now, create our new, gmail compatible, table row
				surprise.append( "\t<tr>\n"
								+ "\t\t<td style=\"width: 120px;\">" + meter + "</td>\n"
								+ "\t\t<td>" + movie + "<br/>" + description	+ "</td>\n"
								+ "\t\t<td>" + opening + "</td>\n"
								+ "\t</tr>\n"
								);
			}

			// close the table and body
			surprise.append("</table></body>\n");
			

			// now send the email (using apache commons email)
			try {
				// Create the email message
				HtmlEmail email = new HtmlEmail();
				email.setHostName("localhost"); // assuming localhost is configured to send
				email.addTo(toEmail, "Überraschungskino Mail Group");
				email.setFrom(user, "Überraschungskino Scanner");
				email.setSubject("Überraschungskino!");
				email.setCharset("UTF-8");

				email.setHtmlMsg(surprise.toString());
				email.setTextMsg("Your email client does not support HTML messages");

				// send the email
				email.send();
			} catch (EmailException mex) {
				mex.printStackTrace();
			}
		} catch (MalformedURLException e) {
			System.out.println("\nMalformed URL: " + e.getMessage());
		} 
		catch (IOException e) {
			System.out.println("\nIO Exception: " + e.getMessage());
		}
	}
}
